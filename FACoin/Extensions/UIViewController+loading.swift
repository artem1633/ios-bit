//
//  UIViewController+loading.swift
//  FACoin
//
//  Created by Алексей Воронов on 21.12.2019.
//  Copyright © 2019 Алексей Воронов. All rights reserved.
//

import UIKit
import NVActivityIndicatorView


extension UIViewController {
    func loadingAnimation(show: Bool) {
        if show {
            if self.view.subviews.first(where: {$0.tag == 2}) == nil {
                let loadingView = NVActivityIndicatorView(frame: CGRect(origin: .zero, size: CGSize(width: 100, height: 100)), type: .ballGridPulse, color: UIColor.BlueColor, padding: nil)
                loadingView.center = self.view.center
                loadingView.tag = 2
                self.view.addSubview(loadingView)
                loadingView.alpha = 0.0
                loadingView.startAnimating()
                UIView.animate(withDuration: 0.3) {
                    loadingView.alpha = 1.0
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    loadingView.center = self.view.center
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.02) {
                    loadingView.center = self.view.center
                }
            }
        } else {
            if let loadingView = self.view.subviews.first(where: {$0.tag == 2}) as? NVActivityIndicatorView {
                UIView.animate(withDuration: 0.3) {
                    loadingView.alpha = 0.0
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                    loadingView.removeFromSuperview()
                }
            }
        }
    }
}
