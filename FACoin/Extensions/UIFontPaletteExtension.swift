//
//  UIFontPaletteExtension.swift
//  FACoin
//
//  Created by Алексей Воронов on 27.11.2019.
//  Copyright © 2019 Алексей Воронов. All rights reserved.
//

import UIKit

extension UIFont {
    static let title = UIFont(name: "ProductSans-Bold", size: 32)!
    static let headerTitleCell = UIFont(name: "ProductSans-Bold", size: 16)!
    static let messageBody = UIFont(name: "ProductSans-Medium", size: 16)!
}
