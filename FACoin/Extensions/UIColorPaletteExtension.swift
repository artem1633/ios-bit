//
//  UIColorPaletteExtension.swift
//  FACoin
//
//  Created by Алексей Воронов on 27.11.2019.
//  Copyright © 2019 Алексей Воронов. All rights reserved.
//

import UIKit

extension UIColor {
    static let BGColor = UIColor(named: "BGColor")!
    static let BlackColor = UIColor(named: "BlackColor")!
    static let BlueColor = UIColor(named: "BlueColor")!
    static let GreenColor = UIColor(named: "GreenColor")!
    static let LightGrayColor = UIColor(named: "LightGrayColor")!
    static let RedColor = UIColor(named: "RedColor")!
    static let TextGrayColor = UIColor(named: "TextGrayColor")!
}
