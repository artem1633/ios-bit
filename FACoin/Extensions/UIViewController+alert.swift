//
//  UIViewController+alert.swift
//  FACoin
//
//  Created by Алексей Воронов on 09.12.2019.
//  Copyright © 2019 Алексей Воронов. All rights reserved.
//

import UIKit
import SwiftMessages

extension UIViewController {
    func showAlert(text: String) {
        let view: MessageView = try! SwiftMessages.viewFromNib(named: "FAMessageView")
        view.configureContent(body: text)
        
        var config = SwiftMessages.Config()
        
        config.presentationStyle = .center
        config.duration = .forever
        config.interactiveHide = false
        config.presentationContext = .window(windowLevel: .alert)
        
        view.buttonTapHandler = {_ in SwiftMessages.hide()}
        
        SwiftMessages.show(config: config, view: view)
    }
    
    func showError(text: String) {
        let view: MessageView = try! SwiftMessages.viewFromNib(named: "FAErrorView")
        
        var config = SwiftMessages.Config()
        
        SwiftMessages.pauseBetweenMessages = 0.0
        
        config.presentationStyle = .top
        config.duration = .automatic
        config.interactiveHide = false
        config.ignoreDuplicates = true
        config.presentationContext = .window(windowLevel: .alert)
        
        view.configureContent(body: text)
        
        SwiftMessages.show(config: config, view: view)
    }
}
