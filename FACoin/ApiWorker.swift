//
//  ApiWorker.swift
//  FACoin
//
//  Created by Алексей Воронов on 09.12.2019.
//  Copyright © 2019 Алексей Воронов. All rights reserved.
//

import Alamofire
import Foundation


class ApiWorker {
    static let shared = ApiWorker()
    private init(){
        token = UserDefaults.standard.string(forKey: "token") ?? ""
    }
    
    var locked = true
    
    var token = "" {
        didSet {
            UserDefaults.standard.set(token ,forKey: "token")
        }
    }
    
    let apiUrl = "http://194.58.120.222/"
    let regEP = "api/user/register"
    let loginEP = "api/user/get-token"
    let restoreEP = "api/user/send-forget-password"
    let FAQEP = "api/faq/index"
    let ticketsEP = "api/ticket/index"
    let ticketAddEP = "api/ticket/create"
    let notifEP = "api/notification/index"
    let messagesEP = "api/ticket/get-messages"
    let sendMessageEP = "api/ticket/send-message"
    let getRatesEP = "api/transaction/rates"
    let getWalletsEP = "api/wallet/index"
    let getTransactionEP = "api/transaction/index"
    let createTransactionEP = "api/transaction/create"
    let uploadImageEP = "api/user/upload-photo"
    let editUserEP = "api/user/edit"
    let fcmToken = "api/user/set-fcm-token"
    
    func apiRequest(url: String, parametrs: [String: String], method: HTTPMethod, completion: @escaping ([String: Any]?, String?) -> ()) {
        AF.request(url, method: method, parameters: parametrs).responseJSON() { responseJSON in
            print(responseJSON.request?.url)
            switch responseJSON.result {
            case .success(let value):
                print(value)
                if let result = value as? [String: Any] {
                    completion(result, nil)
                } else if let result = value as? [[String: Any]] {
                    completion(["result": result], nil)
                } else {
                    completion([:], nil)
                }
            case .failure(let error):
                if error.responseCode == nil {
                    completion(nil, "Нет подключения к интернету")
                } else {
                    completion(nil, "Ошибка сервера")
                }
            }
        }
    }
    
    func regUser(pass: String, email: String, completion: @escaping (String?) -> ()) {
        let url = apiUrl + regEP
        apiRequest(url: url, parametrs: ["email": email, "login": email, "password": pass], method: .post) {
            (value, error) in
            if error != nil {
                completion(error)
            } else {
                if value?["email"] != nil {
                    completion((value?["email"] as? [String])?.first)
                } else if value?["password"] != nil {
                    completion((value?["password"] as? [String])?.first)
                } else if value?["login"] != nil {
                    completion((value?["login"] as? [String])?.first)
                } else if let newToken = value?["result"] as? String {
                    self.token = newToken
                    completion(nil)
                } else {
                    completion("Ошибка регистрации")
                }
            }
        }
    }
    
    func loginUser(login: String, pass: String, completion: @escaping (String?) -> ()) {
        let url = apiUrl + loginEP
        apiRequest(url: url, parametrs: ["login": login, "password": pass], method: .post) {(value, error) in
            if error != nil {
                completion(error)
            } else {
                if let newToken = value?["result"] as? String {
                    self.token = newToken
                    completion(nil)
                } else {
                   completion("Неверный e-mail или пароль")
                }
            }
        }
    }
    
    func restorePass(email: String, completion: @escaping (String?) -> ()) {
        let url = apiUrl + restoreEP
        apiRequest(url: url, parametrs: ["email": email], method: .get) { (value, error) in
            if error != nil {
                completion(error)
            } else {
                if value?["result"] != nil {
                    completion(nil)
                } else {
                    completion("Проверьте введенный email")
                }
            }
        }
    }
    
    func loadFAQ(completion: @escaping ([[String: Any]]) -> ()) {
        let url = apiUrl + FAQEP
        apiRequest(url: url, parametrs: ["token": self.token], method: .get) { (value, error) in
            if error != nil {
                completion([])
            } else {
                print(value)
                if let result = value?["result"] as? [[String: Any]] {
                    completion(result)
                } else {
                    completion([])
                }
            }
        }
    }
    
    let ticketStatus: [Int: String] = [0: "Новый", 1: "В работе", 2: "Завершен", 3: "Отклонен"]
    
    func getTickets(completion: @escaping ([Ticket], String?) -> ()) {
        let url = apiUrl + ticketsEP
        apiRequest(url: url, parametrs: ["token": self.token], method: .get) { (value, error) in
            if error != nil {
                completion([], error!)
            } else {
                if let jsonResult = value?["result"] as? [[String: Any]] {
                    var result: [Ticket] = []
                    for i in jsonResult {
                        var ticket = Ticket()
                        ticket.date = i["created_at"] as? String ?? ""
                        ticket.id = String(i["id"] as! Int)
                        ticket.status = self.ticketStatus[Int(i["status"] as! String)!]
                        ticket.title = i["subject"] as? String ?? ""
                        ticket.text = i["description"] as? String ?? ""
                        result.append(ticket)
                    }
                    completion(result, nil)
                } else {
                    completion([], "Ошибка JSON")
                }
            }
        }
    }
    
    func addTicket(subject: String, description: String, completion: @escaping (String?) -> ()) {
        let url = apiUrl + ticketAddEP
        apiRequest(url: url, parametrs: ["subject": subject, "description": description, "token": self.token], method: .post) { value, error in
            if error != nil {
                completion(error)
            } else {
                if value?["id"] != nil {
                    completion(nil)
                } else {
                    completion("Ошибка создания обращения")
                }
            }
        }
    }
    
    func getNotif(completion: @escaping ([NotificationQ], String?) -> ()) {
        let url = apiUrl + notifEP
        apiRequest(url: url, parametrs: ["token": self.token], method: .get) { (value, error) in
            if error != nil {
                completion([], error!)
            } else {
                if let jsonResult = value?["result"] as? [[String: Any]] {
                    var result: [NotificationQ] = []
                    for i in jsonResult {
                        let notif = NotificationQ(id: String(i["id"] as! Int), subject: i["subject"] as! String, content: i["content"] as! String)
                        result.append(notif)
                    }
                    completion(result, nil)
                    
                } else {
                    completion([], "Ошибка JSON")
                }
            }
        }
    }
    
    func getMessages(postId: String, completion: @escaping ([Message], String?) -> ()) {
        let url = apiUrl + messagesEP
        apiRequest(url: url, parametrs: ["token": self.token, "postId": postId], method: .get) { (value, error) in
            if error != nil {
                completion([], error!)
            } else {
                if let jsonResult = value?["result"] as? [[String: Any]] {
                    var result: [Message] = []
                    for i in jsonResult {
                        let msg = Message(sender: (i["from"] as! Int) == 1, message: i["text"] as! String)
                        result.append(msg)
                    }
                    completion(result, nil)
                    
                } else {
                    completion([], "Ошибка JSON")
                }
            }
        }
    }
    
    func sendMessages(postId: String, text: String, completion: @escaping (String?) -> ()) {
        let url = apiUrl + sendMessageEP
        apiRequest(url: url, parametrs: ["postId": postId, "text": text, "token": self.token], method: .post) { value, error in
            if error != nil {
                completion(error)
            } else {
                if value?["id"] != nil {
                    completion(nil)
                } else {
                    completion("Ошибка создания обращения")
                }
            }
        }
    }
    
    func getRates(completion: @escaping ([String: Double],String?) -> ()) {
        let url = apiUrl + getRatesEP
        apiRequest(url: url, parametrs: ["token": self.token], method: .get) { value, error in
            if error != nil {
                completion([:], error)
            } else {
                if let result: [String: Double] =  value?["btc"] as? [String : Double] {
                    completion(result, nil)
                } else {
                    completion([:], "Ошибка JSON")
                }
            }
        }
    }
    
    func getComission(completion: @escaping ([String: Any],String?) -> ()) {
        let url = apiUrl + getRatesEP
        apiRequest(url: url, parametrs: ["token": self.token], method: .get) { value, error in
            if error != nil {
                completion([:], error)
            } else {
                if let result: [String: Any] =  value?["commission"] as? [String : Any] {
                    completion(result, nil)
                } else {
                    completion([:], "Ошибка JSON")
                }
            }
        }
    }
    
    func getMax(completion: @escaping ([String: Any],String?) -> ()) {
        let url = apiUrl + getRatesEP
        apiRequest(url: url, parametrs: ["token": self.token], method: .get) { value, error in
            if error != nil {
                completion([:], error)
            } else {
                if let result: [String: Any] =  value?["max_rub"] as? [String : Any] {
                    completion(result, nil)
                } else {
                    completion([:], "Ошибка JSON")
                }
            }
        }
    }
    
    func getMin(completion: @escaping ([String: Any],String?) -> ()) {
        let url = apiUrl + getRatesEP
        apiRequest(url: url, parametrs: ["token": self.token], method: .get) { value, error in
            if error != nil {
                completion([:], error)
            } else {
                if let result: [String: Any] =  value?["min_rub"] as? [String : Any] {
                    completion(result, nil)
                } else {
                    completion([:], "Ошибка JSON")
                }
            }
        }
    }
    
    func getWallets(completion: @escaping ([Wallet],String?) -> ()) {
        let url = apiUrl + getWalletsEP
        apiRequest(url: url, parametrs: ["token": self.token], method: .get) { value, error in
            if error != nil {
                completion([], error)
            } else {
                if let resultJSON = value?["result"] as? [[String: Any]] {
                    var result: [Wallet] = []
                    for i in resultJSON {
                        let id = i["id"] as? Int ?? 0
                        let address = i["address"] as? String ?? ""
                        let password = i["password"] as? String ?? ""
                        let balance = Double(i["balance"] as? String ?? "0.0") ?? 0.0
                        let type = i["type"] as? String ?? ""
                        let wallet = Wallet(id: id, address: address, password: password, balance: balance, type: type)
                        result.append(wallet)
                    }
                    completion(result, nil)
                } else {
                    completion([], "Ошибка JSON")
                }
            }
        }
    }
    
    
    func getTransactions(completion: @escaping ([Transaction],String?) -> ()) {
        let url = apiUrl + getTransactionEP
        apiRequest(url: url, parametrs: ["token": self.token], method: .get) { value, error in
            if error != nil {
                completion([], error)
            } else {
                if let resultJSON = value?["result"] as? [[String: Any]] {
                    var result: [Transaction] = []
                    for i in resultJSON {
                        let id = i["id"] as? Int ?? 0
                        let type = i["type"] as? String ?? ""
                        let amount = Double(i["amount"] as? String ?? "") ?? 0.0
                        let created_at = i["created_at"] as? String ?? ""
                        let destination = i["wallet_to"] as? String ?? ""
                        let key = i["key"] as? String ?? ""
                        let transaction = Transaction(id: id, type: type, created_at: self.converDate(string: created_at), amount: amount, destination: destination, key: key)
                        result.append(transaction)
                    }
                    completion(result.reversed(), nil)
                } else {
                    completion([], "Ошибка JSON")
                }
            }
        }
    }
    
    func createTransaction(walletFromId: String, walletTo: String, type: String, amount: String, commission: String, comment: String, completion: @escaping (String?) -> ()) {
        let url = apiUrl + createTransactionEP
        apiRequest(url: url, parametrs: ["token": self.token, "wallet_from_id": walletFromId, "wallet_to": walletTo, "type": type, "amount": amount, "commission": commission, "comment": comment], method: .post) { value, error in
            if error != nil {
                completion(error)
            } else {
                if let result = value {
                    if (result["id"] as? Int) != nil {
                        completion(nil)
                    } else {
                        completion("Ошибка создания транзакции")
                    }
                } else {
                    completion("Ошибка создания транзакции")
                }
            }
        }
    }
    
    
    
    func upload(image: Data, type: Int, completion: @escaping (String?) -> ()) {
        let url = apiUrl + uploadImageEP
        let params: [String: String] = ["token": self.token, "type": "\(type)"]
        AF.upload(multipartFormData: { multiPart in
            for (key, value) in params {
                multiPart.append(value.data(using: .utf8)!, withName: key)
            }
            multiPart.append(image, withName: "file", fileName: "file.png", mimeType: "image/png")
        }, to: url, method: .post)
            .uploadProgress(queue: .main, closure: { progress in
                print("Upload Progress: \(progress.fractionCompleted)")
            })
            .responseJSON(completionHandler: { responseJSON in
                print(responseJSON)
                switch responseJSON.result {
                case .success(let value):
                    print(value)
                    completion(nil)
                case .failure(let error):
                    completion(error.errorDescription)
                }
            })
    }
    
    
    func editUser(name: String, country: String, completion: @escaping (String?) -> ()) {
        let url = apiUrl + editUserEP
        apiRequest(url: url, parametrs: ["token": self.token, "name": name, "country": country], method: .post) { value, error in
            if error != nil {
                completion(error)
            } else {
                if let result = value {
                    if (result["id"] as? Int) != nil {
                        completion(nil)
                    } else {
                        completion("Ошибка добавления данных")
                    }
                } else {
                    completion("Ошибка добавления данных")
                }
            }
        }
    }
    
    func editUser(name: String, email: String, completion: @escaping (String?) -> ()) {
        let url = apiUrl + editUserEP
        apiRequest(url: url, parametrs: ["token": self.token, "name": name, "email": email], method: .post) { value, error in
            if error != nil {
                completion(error)
            } else {
                if let result = value {
                    if (result["id"] as? Int) != nil {
                        completion(nil)
                    } else {
                        completion("Ошибка добавления данных")
                    }
                } else {
                    completion("Ошибка добавления данных")
                }
            }
        }
    }
    
    func editEmail(email: String, completion: @escaping (String?) -> ()) {
        let url = apiUrl + editUserEP
        apiRequest(url: url, parametrs: ["token": self.token, "email": email], method: .post) { value, error in
            if error != nil {
                completion(error)
            } else {
                if let result = value {
                    if (result["id"] as? Int) != nil {
                        completion(nil)
                    } else {
                        completion("Ошибка изменения данных")
                    }
                } else {
                    completion("Ошибка изменения данных")
                }
            }
        }
    }
    
    
    func converDate(string: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: string) ?? Date()
        return date
    }
    
    func sendFCM(completion: @escaping (String?) -> ()) {
        print(UserDefaults.standard.string(forKey: "userFCMToken"))
        print()
        let url = apiUrl + fcmToken
        apiRequest(url: url, parametrs: ["fcm_token": UserDefaults.standard.string(forKey: "userFCMToken") ?? "","token": self.token], method: .post) { value, error in
            if error != nil {
                completion(error)
            } else {
                print(UserDefaults.standard.string(forKey: "userFCMToken"))
                print(value)
            }
        }
    }
    
}

struct Wallet {
    var id: Int
    var address: String
    var password: String
    var balance: Double
    var type: String
}


struct Transaction {
    var id: Int
    var type: String
    var created_at: Date
    var amount: Double
    var destination: String
    var key: String
}
