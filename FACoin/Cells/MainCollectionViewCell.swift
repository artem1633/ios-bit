//
//  MainCollectionViewCell.swift
//  FACoin
//
//  Created by Алексей Воронов on 04.12.2019.
//  Copyright © 2019 Алексей Воронов. All rights reserved.
//

import UIKit

class MainCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var courceLabel: UILabel!
    @IBOutlet weak var reserveLabel: UILabel!
    
    var delegate: MainCollectionViewCellDelegate?
    
    @IBAction func buyAction() {
        delegate?.buyAction()
    }
    
    @IBAction func sendAction() {
        delegate?.sendAction()
    }
}


protocol MainCollectionViewCellDelegate {
    func buyAction()
    func sendAction()
}
