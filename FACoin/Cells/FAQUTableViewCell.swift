//
//  FAQUTableViewCell.swift
//  FACoin
//
//  Created by Алексей Воронов on 09.12.2019.
//  Copyright © 2019 Алексей Воронов. All rights reserved.
//

import UIKit

class FAQUTableViewCell: UITableViewCell {
    
    @IBOutlet weak var label: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
