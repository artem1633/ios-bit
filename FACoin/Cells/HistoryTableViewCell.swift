//
//  HistoryTableViewCell.swift
//  FACoin
//
//  Created by Алексей Воронов on 04.12.2019.
//  Copyright © 2019 Алексей Воронов. All rights reserved.
//

import UIKit

class HistoryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var destinationLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setup(transaction: Transaction) {
        if transaction.type == "transfer" {
            iconImageView.image = #imageLiteral(resourceName: "Tranfer")
            valueLabel.textColor = UIColor.RedColor
            titleLabel.text = "Перевод"
        } else {
            iconImageView.image = #imageLiteral(resourceName: "Purchase")
            valueLabel.textColor = UIColor.GreenColor
            titleLabel.text = "Покупка"
        }
        valueLabel.text = String(format: "%.8f", transaction.amount) + " BTC"
        destinationLabel.text = transaction.destination
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "ru_RU")
        formatter.dateFormat = "HH:mm"
        timeLabel.text = formatter.string(from: transaction.created_at)
    }
}
