//
//  SupportTableViewCell.swift
//  FACoin
//
//  Created by Алексей Воронов on 11.12.2019.
//  Copyright © 2019 Алексей Воронов. All rights reserved.
//

import UIKit

class SupportTableViewCell: UITableViewCell {
    
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
