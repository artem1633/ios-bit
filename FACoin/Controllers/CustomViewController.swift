//
//  CustomViewController.swift
//  FACoin
//
//  Created by Алексей Воронов on 01.12.2019.
//  Copyright © 2019 Алексей Воронов. All rights reserved.
//

import UIKit
import SwiftMessages

class CustomViewController: UIViewController {
    
    @IBInspectable var isNavBarHidden: Bool = false
    @IBInspectable var statusBarStyle: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        
//        let view: MessageView = try! SwiftMessages.viewFromNib(named: "FACardField")
//        var config = SwiftMessages.Config()
//        config.interactiveHide = true
//        config.duration = .forever
//        config.presentationStyle = .bottom
//        config.presentationContext = .viewController(self)
//        
//        SwiftMessages.show(config: config, view: view)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(isNavBarHidden, animated: false)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return UIStatusBarStyle.init(rawValue: statusBarStyle) ?? UIStatusBarStyle.default
    }

}
