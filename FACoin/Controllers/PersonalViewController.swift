//
//  PersonalViewController.swift
//  FACoin
//
//  Created by Алексей Воронов on 18.12.2019.
//  Copyright © 2019 Алексей Воронов. All rights reserved.
//

import UIKit
import SPAlert

class PersonalViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var fioTextField: UITextField!
    @IBOutlet weak var countryField: UITextField!
    @IBOutlet weak var docTypeSegmentedControl: UISegmentedControl!
    @IBOutlet weak var photoImageView: UIImageView!
    
    let api = ApiWorker.shared
    var photoUploaded = false
    
    @IBAction func addPhoto() {
        let pickerController = UIImagePickerController()
        pickerController.delegate = self
        pickerController.allowsEditing = false
        pickerController.mediaTypes = ["public.image"]
        pickerController.sourceType = .camera
        
        self.present(pickerController, animated: true)
    }
    
    @IBAction func acceptAndContinue() {
        guard photoUploaded == true else {showError(text: "Загрузите фото документа"); return}
        guard let fio = fioTextField.text else {showError(text: "Введите ФИО"); return}
        guard let country = countryField.text else {showError(text: "Введите Страну"); return}
        self.loadingAnimation(show: true)
        api.editUser(name: fio, country: country) { error in
            if error != nil {
                self.showError(text: error!)
            } else {
                self.performSegue(withIdentifier: "main", sender: nil)
            }
            self.loadingAnimation(show: false)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        hideKeyboardWhenTappedAround(moveView: false)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        if let image = info[.originalImage] as? UIImage {
            print(image.jpegData(compressionQuality: 0.5)!.count)
            self.loadingAnimation(show: true)
            api.upload(image: image.jpegData(compressionQuality: 0.9)!, type: docTypeSegmentedControl.selectedSegmentIndex) { error in
                if error != nil {
                    self.showError(text: error!)
                } else {
                    SPAlert.present(title: "Фото загружено", preset: .done)
                    self.photoImageView.image = image
                    self.photoUploaded = true
                }
                self.loadingAnimation(show: false)
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
