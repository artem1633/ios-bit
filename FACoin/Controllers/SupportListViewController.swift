//
//  SupportListViewController.swift
//  FACoin
//
//  Created by Алексей Воронов on 11.12.2019.
//  Copyright © 2019 Алексей Воронов. All rights reserved.
//

import UIKit

class SupportListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var tickets: [Ticket] = []
    let api = ApiWorker.shared
    var selectedId = ""
    var selectedText = ""
    
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadingAnimation(show: true)

        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.tableFooterView = UIView(frame: CGRect(origin: .zero, size: CGSize(width: 0, height: 100)))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        api.getTickets() { (result, error) in
            if error != nil {
                self.showError(text: error!)
            } else {
                self.tickets = result
                self.tableView.reloadData()
            }
            self.loadingAnimation(show: false)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? AddTicketViewController {
            destinationVC.closeAction = {
                self.api.getTickets() { (result, error) in
                    if error != nil {
                        self.showError(text: error!)
                    } else {
                        self.tickets = result
                        self.tableView.reloadData()
                    }
                }
            }
        } else if let destinationVC = segue.destination as? SupportViewController {
            destinationVC.selectedId = self.selectedId
            destinationVC.text = self.selectedText
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tickets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let ticket = tickets[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! SupportTableViewCell
        cell.dateLabel.text = ticket.date
        cell.idLabel.text = "№\(ticket.id ?? "")"
        cell.statusLabel.text = ticket.status
        cell.titleLabel.text = ticket.title
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        let footerView = view as! UITableViewHeaderFooterView
        footerView.contentView.backgroundColor = UIColor.BGColor
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 16
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedId = tickets[indexPath.row].id
        selectedText = tickets[indexPath.row].text
        performSegue(withIdentifier: "support", sender: nil)
    }
    

}


struct Ticket {
    var id: String!
    var status: String!
    var title: String!
    var date: String!
    var text: String!
}
