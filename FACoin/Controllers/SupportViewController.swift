//
//  SupportViewController.swift
//  FACoin
//
//  Created by Алексей Воронов on 01.12.2019.
//  Copyright © 2019 Алексей Воронов. All rights reserved.
//

import UIKit
import MessengerKit

class SupportViewController: MSGMessengerViewController, MSGDelegate, MSGDataSource {
    
    let me = User(displayName: "Я", avatar: nil, isSender: true)
    let support = User(displayName: "Поддержка", avatar: nil, isSender: false)
    var selectedId = ""
    var text = ""
    let api = ApiWorker.shared
    var apiMessages: [Message] = []
    var updateTimer: Timer?
    
    lazy var messages: [[MSGMessage]] = [[MSGMessage(id: 0, body: .text(self.text), user: self.me, sentAt: Date())]]
    
    override var style: MSGMessengerStyle {
        var style = MessengerKit.Styles.iMessage
        style.inputPlaceholder = "Сообщение"
        style.font = UIFont.messageBody
        style.inputFont = UIFont.messageBody
        style.incomingTextColor = UIColor.BlackColor
        style.backgroundColor = UIColor.BGColor
        style.incomingBubbleColor = UIColor.white
        return style
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadingAnimation(show: true)
        
        self.dataSource = self
        
        self.view.backgroundColor = UIColor.BGColor
        
        self.hideKeyboardWhenTappedAround(moveView: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getMessages()
        updateTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(getMessages), userInfo: nil, repeats: true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        updateTimer?.invalidate()
    }
    
    @objc func getMessages() {
        api.getMessages(postId: selectedId) { newMessages, error in
            if error != nil {
                self.showError(text: error!)
            } else {
                if newMessages.count > 0 {
                    self.apiMessages = newMessages
                    
                    self.messages[0] = [MSGMessage(id: 0, body: .text(self.text), user: self.me, sentAt: Date())]
                    self.messages[0].removeFirst()
                    for (index, i) in self.apiMessages.enumerated() {
                        let msg = MSGMessage(id: index, body: .text(i.message), user: {if i.sender {return self.me} else {return self.support}}(), sentAt: Date())
                        self.messages[0].append(msg)
                        //if self.messages.contains([MSGMessage(id: 0, body: .text("example first message that can't consists"), user: self.me, sentAt: Date())]) {
                            //print("CONTAINS")
                        //}
                    }
                    self.collectionView.reloadData()
                }
            }
            self.loadingAnimation(show: false)
        }
    }
    
    func numberOfSections() -> Int {
        return 1
    }
    
    func numberOfMessages(in section: Int) -> Int {
        return messages[0].count
    }
    
    func message(for indexPath: IndexPath) -> MSGMessage {
        return messages[0][indexPath.item]
    }
    
    func footerTitle(for section: Int) -> String? {
        return ""
    }
    
    func headerTitle(for section: Int) -> String? {
        return ""
    }
    
    override func inputViewPrimaryActionTriggered(inputView: MSGInputView) {
        if inputView.message != "" {
            api.sendMessages(postId: self.selectedId, text: inputView.message) { error in
                if error != nil {
                    self.showError(text: error!)
                } else {
                    self.getMessages()
                }
            }
        } else {
            showError(text: "Введите сообщение")
        }
    }

}


struct User: MSGUser {
    var displayName: String = ""
    var avatar: UIImage?
    var isSender: Bool = false
}

struct Message {
    var sender: Bool
    var message: String
}
