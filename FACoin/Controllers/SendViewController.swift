//
//  SendViewController.swift
//  FACoin
//
//  Created by Алексей Воронов on 15.12.2019.
//  Copyright © 2019 Алексей Воронов. All rights reserved.
//

import UIKit
import SPAlert

class SendViewController: UIViewController {
    
    @IBOutlet weak var destinationTextField: UITextField!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var comissionLabel: UILabel!
    
    
    let api = ApiWorker.shared
    var wallet: Wallet?
    var minRub : Double = 0
    var comission : Double = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadingAnimation(show: true)

        hideKeyboardWhenTappedAround(moveView: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        getWallets()
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func getWallets() {
        api.getComission() { (result, error) in
            if error != nil {
                self.showError(text: error!)
            } else {
                self.comissionLabel.text = "Комисия за перевод BTC: \(result["transactionValue"] ?? "0")"
            }
        }
        api.getMin() { (result, error) in
            if error != nil {
                self.showError(text: error!)
            } else {
                if result["transferValue"] != nil {
                    self.minRub = Double(result["transferValue"] as! Double)
                } else {
                    self.showError(text: "Ошибка получения данных")
                }
            }
            self.loadingAnimation(show: false)
        }
        api.getWallets() { (result, error) in
            if error != nil {
                self.showError(text: error!)
            } else {
                self.wallet = result.first
                self.balance = self.wallet?.balance ?? 0
                self.balanceLabel.text = "Ваш баланс: \(String(format: "%.8f", self.balance ?? "")) BTC"
            }
            self.loadingAnimation(show: false)
        }
    }
    
    var balance : Double = 0
    
    var isSending = true
    
    @IBAction func sendTranaction() {
        if isSending {
            if let walletFrom = self.wallet {
                var number : Double = 0
                if Double(amountTextField.text ?? "0") != nil {
                    number = Double(amountTextField.text ?? "0")!
                } else {
                    let formatter = NumberFormatter()
                    formatter.decimalSeparator = ","
                    let grade = formatter.number(from: amountTextField.text ?? "0")
                    if let doubleGrade = grade?.doubleValue {
                        number = doubleGrade
                    }
                }
                if number + comission <= balance {
                    if number >= minRub {
                        isSending = false
                        api.createTransaction(walletFromId: String(walletFrom.id), walletTo: destinationTextField.text ?? "", type: "transfer", amount: "\(number)", commission: "\(comission)", comment: "") { error in
                            if error != nil {
                                self.showError(text: error!)
                            } else {
                                self.isSending = true
                                self.navigationController?.popViewController(animated: true)
                                SPAlert.present(title: "Отправлено", preset: .done)
                            }
                        }
                    } else {
                        self.showError(text: "Минимальная сумма перевода \(minRub)")
                    }
                } else {
                    self.showError(text: "Сумма не должна превышать баланса")
                }
            }
        }
    }
}
