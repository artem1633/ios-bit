//
//  AddTicketViewController.swift
//  FACoin
//
//  Created by Алексей Воронов on 11.12.2019.
//  Copyright © 2019 Алексей Воронов. All rights reserved.
//

import UIKit

class AddTicketViewController: UIViewController {
    
    let api = ApiWorker.shared
    var closeAction = {}
    
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var textField: UITextField!
    
    @IBAction func addAction() {
        if textField.text!.count == 0 {
            showError(text: "Введите тему обращения")
            return
        }
        if textView.text!.count == 0 {
            showError(text: "Опишите проблему")
            return
        }
        api.addTicket(subject: textField.text!, description: textView.text!) { error in
            if error != nil {
                self.showError(text: error!)
            } else {
                self.closeAction()
                self.dismiss(animated: true, completion: nil)
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        textView.placeholder = "Опишите подробнее проблему, чтобы мы могли вам помочь..."
    }
}
