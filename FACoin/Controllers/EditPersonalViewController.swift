//
//  EditPersonalViewController.swift
//  FACoin
//
//  Created by Алексей Воронов on 21.12.2019.
//  Copyright © 2019 Алексей Воронов. All rights reserved.
//

import UIKit
import SPAlert

class EditPersonalViewController: UIViewController {
    
    let api = ApiWorker.shared
    
    @IBOutlet weak var fioTextField: UITextField!
    @IBOutlet weak var emailField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround(moveView: false)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func saveAction() {
        guard let fio = fioTextField.text else {showError(text: "Введите ФИО"); return}
        guard let email = emailField.text else {showError(text: "Введите Email"); return}
        self.loadingAnimation(show: true)
        api.editUser(name: fio, email: email) { error in
            if error != nil {
                self.showError(text: error!)
            } else {
                SPAlert.present(title: "Данные изменены", preset: .done)
                self.navigationController?.popViewController(animated: true)
            }
            self.loadingAnimation(show: false)
        }
    }
}
