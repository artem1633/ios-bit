//
//  LoginViewController.swift
//  FACoin
//
//  Created by Алексей Воронов on 01.12.2019.
//  Copyright © 2019 Алексей Воронов. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    let api = ApiWorker.shared
    
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passTextField: UITextField!
    
    
    @IBAction func loginAction() {
        if loginTextField.text != "" && passTextField.text != "" {
            self.loadingAnimation(show: true)
            api.loginUser(login: loginTextField.text!, pass: passTextField.text!) { error in
                if error == nil {
                    self.performSegue(withIdentifier: "main", sender: nil)
                } else {
                    self.showError(text: error!)
                }
                self.loadingAnimation(show: false)
            }
        } else {
            self.showError(text: "Должны быть заполненны все поля")
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
}
