//
//  RegViewController.swift
//  FACoin
//
//  Created by Алексей Воронов on 01.12.2019.
//  Copyright © 2019 Алексей Воронов. All rights reserved.
//

import UIKit

class RegViewController: UIViewController {
    
    //@IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passTextField: UITextField!
    
    let api = ApiWorker.shared

    @IBAction func loginAction() {
        if emailTextField.text != "" && passTextField.text != "" && isChecked{
            self.loadingAnimation(show: true)
            api.regUser(pass: passTextField.text!, email: emailTextField.text!) { error in
                if error == nil {
                    self.navigationController?.setNavigationBarHidden(true, animated: true)
                    self.performSegue(withIdentifier: "next", sender: nil)
                } else {
                    self.showError(text: error!)
                }
                self.loadingAnimation(show: false)
            }
        } else {
            self.showError(text: "Должны быть заполненны все поля")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        hideKeyboardWhenTappedAround(moveView: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    @IBOutlet weak var checkBox: UIButton!
    var isChecked = false
    
    @IBAction func checkBoxTapped(_ sender: Any) {
        if #available(iOS 13.0, *) {
            checkBox.setImage(UIImage(systemName: "checkmark.square"), for: .normal)
        }
        isChecked = true
    }
    @IBAction func agreementTapped(_ sender: Any) {
        
    }
    
    
    
}
