//
//  RestoreViewController.swift
//  FACoin
//
//  Created by Алексей Воронов on 09.12.2019.
//  Copyright © 2019 Алексей Воронов. All rights reserved.
//

import UIKit

class RestoreViewController: UIViewController {
    
    let api = ApiWorker.shared
    
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBAction func restoreAction() {
        if emailTextField.text == "" {
            showError(text: "Заполните поле email")
        } else {
            api.restorePass(email: emailTextField.text!) { error in
                if error != nil {
                    self.showError(text: error!)
                } else {
                    self.showAlert(text: "На электронную почту было отправлено письмо с ссылкой для восстановления пароля")
                }
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        hideKeyboardWhenTappedAround(moveView: false)
    }

}
