//
//  SettingsViewController.swift
//  FACoin
//
//  Created by Алексей Воронов on 01.12.2019.
//  Copyright © 2019 Алексей Воронов. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    
    let api = ApiWorker.shared

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func exitAction() {
        let alert = UIAlertController(title: "Выход", message: "Вы действительно хотите выйти?", preferredStyle: .alert)
        let actionAccept = UIAlertAction(title: "Да", style: .default) { _ in
            UserDefaults.standard.set("", forKey: "token")
            self.api.token = ""
            self.performSegue(withIdentifier: "main", sender: nil)
        }
        
        let actionDismiss = UIAlertAction(title: "Нет", style: .cancel) { _ in
            
        }
        
        alert.addAction(actionAccept)
        alert.addAction(actionDismiss)
        
        present(alert, animated: true)
    }

}
