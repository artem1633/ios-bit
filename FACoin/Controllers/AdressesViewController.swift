//
//  AdressesViewController.swift
//  FACoin
//
//  Created by Алексей Воронов on 23.12.2019.
//  Copyright © 2019 Алексей Воронов. All rights reserved.
//

import UIKit
import SwiftMessages
import SPAlert

class AdressesViewController: UIViewController, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    var adresses: [String] = UserDefaults.standard.array(forKey: "adr") as? [String] ?? []
    var cardView: FAMessageFieldView?

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return adresses.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == adresses.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "add") as! AdressesActionAddCell
            cell.callBack = {
                self.addAdress()
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "adr") as! AdressesCell
            cell.label.text = adresses[indexPath.row]
            return cell
        }
    }
    
    func addAdress() {
        cardView = try! SwiftMessages.viewFromNib(named: "FAMessageFieldView")
        var config = SwiftMessages.Config()
        config.interactiveHide = true
        config.duration = .forever
        config.presentationStyle = .center
        config.presentationContext = .viewController(self)
        
        cardView?.layer.shadowColor = UIColor.black.cgColor
        cardView?.layer.shadowOffset = CGSize(width: 0, height: 10)
        cardView?.layer.shadowOpacity = 0.15
        cardView?.layer.shadowRadius = 40
                        
        SwiftMessages.show(config: config, view: cardView!)
        
        cardView?.actionCallback = {
            if let adress = self.cardView?.textField.text {
                self.adresses.append(adress)
                UserDefaults.standard.set(self.adresses, forKey: "adr")
                SwiftMessages.hideAll()
                self.tableView.reloadData()
            }
        }
    }

}

class AdressesCell: UITableViewCell {
    @IBOutlet weak var label: UILabel!
    @IBAction func copyAction() {
        if let adress = label.text {
            UIPasteboard.general.string = adress
            SPAlert.present(title: "Скопированно", preset: .doc)
        }
    }
}

class AdressesActionAddCell: UITableViewCell {
    var callBack: (()->())?
    @IBAction func addAction() {
        callBack?()
    }
}
