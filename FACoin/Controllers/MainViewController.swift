//
//  ViewController.swift
//  FACoin
//
//  Created by Алексей Воронов on 27.11.2019.
//  Copyright © 2019 Алексей Воронов. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDataSource, UITableViewDelegate, MainCollectionViewCellDelegate {
    func buyAction() {
        performSegue(withIdentifier: "buy", sender: nil)
    }
    
    func sendAction() {
        performSegue(withIdentifier: "send", sender: nil)
        
       
    }
    
    @IBAction func allTransactionsAction() {
        self.tabBarController?.selectedIndex = 1
    }
    
    
    let api = ApiWorker.shared
    var rate: [String: Double] = [:]
    var wallets: [Wallet] = []
    var transactions: [Transaction] = []
    var updateTimer: Timer?
    
    @IBOutlet weak var mainCollectionView: UICollectionView!
    @IBOutlet weak var hitoryTableView: UITableView!
    
    @IBOutlet weak var buttonCoin1: UIButton!
    @IBOutlet weak var buttonCoin2: UIButton!
    @IBOutlet weak var buttonCoin3: UIButton!
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! MainCollectionViewCell
        cell.titleLabel.text = "\(String(format: "%.8f", wallets.first?.balance ?? "")) BTC"
        cell.courceLabel.text = "1 BTC = \(String(format: "%.2f", rate["valueBuy"] ?? "")) ₽"
        cell.reserveLabel.text = "Резерв BTC: \(String(format: "%.8f", rate["baseBalance"] ?? ""))"
        cell.delegate = self
        return cell
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getTransctionsRows(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HistoryTableViewCell
        cell.setup(transaction: getTransaction(section: indexPath.section, row: indexPath.row))
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return getTransactionsSections()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "ru_RU")
        formatter.dateFormat = "dd MMMM"
        
        let date = getSectionDate(section: section)
        
        return formatter.string(from: date)
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let headerView = view as! UITableViewHeaderFooterView
        headerView.contentView.backgroundColor = .white
        headerView.backgroundColor = UIColor.clear
        headerView.textLabel?.font = UIFont.headerTitleCell
        headerView.textLabel?.textColor = UIColor.TextGrayColor
    }
    
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        let footerView = view as! UITableViewHeaderFooterView
        footerView.contentView.backgroundColor = UIColor.BGColor
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 16
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 46
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let transaction = getTransaction(section: indexPath.section, row: indexPath.row)
        print(transaction)
        if transaction.type == "transfer" {
            //print("https://www.blockchain.com/btc/tx/\(transaction.key)")
            guard let url = URL(string: "https://www.blockchain.com/btc/tx/\(transaction.key)") else { return }
            UIApplication.shared.open(url)
        }
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        loadingAnimation(show: true)
        
        mainCollectionView.delegate = self
        mainCollectionView.dataSource = self
        
        hitoryTableView.delegate = self
        hitoryTableView.dataSource = self
        
        hitoryTableView.backgroundColor = UIColor.BGColor
        
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationItem.backBarButtonItem = nil
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        mainCollectionView.scrollToItem(at: IndexPath(item: 0, section: 0), at: .centeredHorizontally, animated: false)
    }
    
    @objc func getRates() {
        api.getRates() { (result, error) in
            if error != nil {
                self.showError(text: error!)
            } else {
                self.rate = result
                self.mainCollectionView.reloadData()
            }
            self.getWallets()
        }
    }
    
    func getWallets() {
        api.getWallets() { (result, error) in
            if error != nil {
                self.showError(text: error!)
            } else {
                self.wallets = result
                self.mainCollectionView.reloadData()
            }
            self.getTransactoins()
        }
    }
    
    func getTransactoins() {
        api.getTransactions() { (result, error) in
            if error != nil {
                self.showError(text: error!)
            } else {
                self.transactions = result
                self.hitoryTableView.reloadData()
            }
            self.loadingAnimation(show: false)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        if api.token == "" {
            performSegue(withIdentifier: "login", sender: nil)
        } else {
            self.getRates()
        }
        
        if api.locked {
            print(api.locked)
            performSegue(withIdentifier: "pin", sender: nil)
        }
        
        updateTimer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(getRates), userInfo: nil, repeats: true)
    }
        
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        updateTimer?.invalidate()
    }
    
    @IBAction func selectCoin(sender: UIButton) {
        //mainCollectionView.scrollToItem(at: IndexPath(item: sender.tag, section: 0), at: .centeredHorizontally, animated: true)
        //setCoinButtonActive(tag: sender.tag)
    }
    
    func setCoinButtonActive(tag: Int) {
        buttonCoin1.setTitleColor(UIColor.BlackColor, for: .normal)
        buttonCoin2.setTitleColor(UIColor.BlackColor, for: .normal)
        buttonCoin3.setTitleColor(UIColor.BlackColor, for: .normal)
        
        switch tag {
        case 0:
            buttonCoin1.setTitleColor(UIColor.BlueColor, for: .normal)
        case 1:
            buttonCoin2.setTitleColor(UIColor.BlueColor, for: .normal)
        case 2:
            buttonCoin3.setTitleColor(UIColor.BlueColor, for: .normal)
        default:
            return
        }
    }
    
    func setup() {
        buttonCoin1.isHidden = true
        //buttonCoin2.isHidden = true
        buttonCoin3.isHidden = true
    }

    func getTransactionsSections() -> Int {
        let calendar = Calendar.current
        var days: [DateComponents] = []
        for transaction in self.transactions {
            let dayComponent = calendar.dateComponents([.day], from: transaction.created_at)
            if !days.contains(dayComponent) { days.append(dayComponent)}
        }
        return days.count
    }
    
    func getTransctionsRows(section: Int) -> Int {
        let calendar = Calendar.current
        var days: [DateComponents] = []
        for transaction in self.transactions {
            let dayComponent = calendar.dateComponents([.day], from: transaction.created_at)
            if !days.contains(dayComponent) { days.append(dayComponent)}
        }
        var result: [Transaction] = []
        for transaction in self.transactions {
            let dayComponent = calendar.dateComponents([.day], from: transaction.created_at)
            if days[section] == dayComponent { result.append(transaction)}
        }
        
        return result.count
    }
    
    func getTransaction(section: Int, row: Int) -> Transaction {
        let calendar = Calendar.current
        var days: [DateComponents] = []
        for transaction in self.transactions {
            let dayComponent = calendar.dateComponents([.day], from: transaction.created_at)
            if !days.contains(dayComponent) { days.append(dayComponent)}
        }
        var result: [Transaction] = []
        for transaction in self.transactions {
            let dayComponent = calendar.dateComponents([.day], from: transaction.created_at)
            if days[section] == dayComponent { result.append(transaction)}
        }
        return result[row]
    }
    
    func getSectionDate(section: Int) -> Date {
        let calendar = Calendar.current
        var days: [DateComponents] = []
        for transaction in self.transactions {
            let dayComponent = calendar.dateComponents([.day], from: transaction.created_at)
            if !days.contains(dayComponent) { days.append(dayComponent)}
        }
        
        for transaction in self.transactions {
            let dayComponent = calendar.dateComponents([.day], from: transaction.created_at)
            if days[section] == dayComponent {
                return transaction.created_at
            }
        }
        return Date()
    }

}


struct NotificationQ {
    var id: String
    var subject: String
    var content: String
}
