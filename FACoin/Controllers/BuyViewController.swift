//
//  BuyViewController.swift
//  FACoin
//
//  Created by Алексей Воронов on 15.12.2019.
//  Copyright © 2019 Алексей Воронов. All rights reserved.
//

import UIKit
import SwiftMessages

class BuyViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var rublesTextField: UITextField!
    @IBOutlet weak var amountTextField: UITextField!
    @IBOutlet weak var reserveLabel: UILabel!
    @IBOutlet weak var courseLabel: UILabel!
    
    var valueBuy: Double = 0.0
    var baseBalance: Double = 0.0
    var cardView: CardMessageView?
    let viewMessages = SwiftMessages()
    
    var maxRub = 0
    var minRub = 0
    
    let api = ApiWorker.shared

    override func viewDidLoad() {
        super.viewDidLoad()
        //registerKB()
        self.loadingAnimation(show: true)
        

        rublesTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        amountTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        hideKeyboardWhenTappedAround(moveView: false)
    }
    

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        
        getRates()
    }
    
    func getRates() {
        api.getRates() { (result, error) in
            if error != nil {
                self.showError(text: error!)
            } else {
                if result["valueBuy"] != nil && result["baseBalance"] != nil {
                    self.valueBuy = result["valueBuy"]!
                    self.baseBalance = result["baseBalance"]!
                    
                    self.reserveLabel.text = "Для вас зарезервировано: \(String(format: "%.8f", self.baseBalance ?? "")) BTC"
                    self.courseLabel.text = "1 BTC = \(String(format: "%.2f", self.valueBuy ?? "")) ₽"
                } else {
                    self.showError(text: "Ошибка получения данных")
                }
            }
            self.loadingAnimation(show: false)
        }
        
        api.getMax() { (result, error) in
            if error != nil {
                self.showError(text: error!)
            } else {
                if result["buyValue"] != nil {
                    self.maxRub = Int(result["buyValue"] as! Double)
                } else {
                    self.showError(text: "Ошибка получения данных")
                }
            }
            self.loadingAnimation(show: false)
        }
        
        api.getMin() { (result, error) in
            if error != nil {
                self.showError(text: error!)
            } else {
                if result["buyValue"] != nil {
                    self.minRub = Int(result["buyValue"] as! Double)
                } else {
                    self.showError(text: "Ошибка получения данных")
                }
            }
            self.loadingAnimation(show: false)
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.tag == 1 {
            if let value1 = rublesTextField.text {
                if let value2 = Double(value1) {
                    if value2 / valueBuy > 0.00000001 {
                        amountTextField.text = String(format: "%.8f", value2 / valueBuy)
                    }
                }
            }
        } else {
            if let value1 = amountTextField.text {
                if let value2 = Double(value1) {
                    if value2 * valueBuy > 0.00000001 {
                        rublesTextField.text = String(format: "%.2f", value2 * valueBuy)
                    }
                }
            }
        }
        if textField.text == "" {
            rublesTextField.text = ""
            amountTextField.text = ""
        }
    }
    
//    @IBAction func payApplePay() {
//
//    }
    
    @available(iOS 13.0, *)
    @IBAction func payCardPay() {
        var number : Double = 0
        var numberBTC : Double = 0
        if Double(rublesTextField.text ?? "0") != nil {
            number = Double(rublesTextField.text ?? "0")!
        } else {
            let formatter = NumberFormatter()
            formatter.decimalSeparator = ","
            let grade = formatter.number(from: rublesTextField.text ?? "0")
            if let doubleGrade = grade?.doubleValue {
                number = doubleGrade
            }
        }
        if Double(amountTextField.text ?? "0") != nil {
            numberBTC = Double(amountTextField.text ?? "0")!
        } else {
            let formatter = NumberFormatter()
            formatter.decimalSeparator = ","
            let grade = formatter.number(from: amountTextField.text ?? "0")
            if let doubleGrade = grade?.doubleValue {
                numberBTC = doubleGrade
            }
        }
        print(number)
        if number.truncatingRemainder(dividingBy: 100) == 0 {
            print(numberBTC, baseBalance)
            if numberBTC <= baseBalance {
                if Int(number) <= maxRub {
                    if Int(number) >= minRub {
                        if Float(amountTextField.text ?? "") != nil {
                            //print(ApiWorker.shared.token, "//////", amountTextField.text)
                            let vc = self.storyboard?.instantiateViewController(identifier: "CardBuyViewController") as! CardBuyViewController
                            vc.urlString = "http://electricshop1.ru/widget_pay/widget.php?token=\(ApiWorker.shared.token)&btc=\(amountTextField.text ?? "")&pay=\(rublesTextField.text ?? "")"
                            print(vc.urlString)
                            self.navigationController?.pushViewController(vc, animated: true)
                        }
                    } else {
                        showError(text: "Минимальная сумма оплаты \(minRub)P")
                    }
                } else {
                    showError(text: "Максимальная сумма оплаты \(maxRub)P")
                }
            } else {
                showError(text: "Сумма не должна превышать резерва")
            }
        } else {
            showError(text: "Введите сумму кратную 100 рублей")
        }
        
        //cardView = try! SwiftMessages.viewFromNib(named: "FACardField")
        //var config = SwiftMessages.Config()
        //config.interactiveHide = true
        //config.duration = .forever
        //config.presentationStyle = .bottom
        //config.presentationContext = .viewController(self)
        //let keyboardTrackingView = KeyboardTrackingView(frame: self.view.bounds)
        //cardView?.addSubview(keyboardTrackingView)
        //config.keyboardTrackingView = keyboardTrackingView
                        
        //viewMessages.show(config: config, view: cardView!)
        
        //cardView?.actionCallback = {
        //    self.checkFields()
        //}
    }
    
    func checkFields() {
        if cardView?.cardNumberField.text == "" {
            showError(text: "Заполните номер карты")
        } else if cardView?.cardHolderField.text == "" {
            showError(text: "Заполните владельца карты")
        } else if cardView?.cardDateField.text == "" {
            showError(text: "Заполните дату действия карты")
        } else if cardView?.cardCVVField.text == "" {
            showError(text: "Заполните CVV код карты")
        }
    }
    
    func registerKB(){
        NotificationCenter.default.addObserver(self, selector: #selector(kbWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(kbWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    var isKeyBoardShown = false
    
    @IBOutlet weak var buyButton: UIButton!
    
    @objc func kbWillShow(notification: NSNotification) {
        print("kb is shown")
        if !isKeyBoardShown {
            if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                UIView.animate(withDuration: 0.3) {
                    self.buyButton.center.y = 200//keyboardSize.height + 20
                }
            }
            isKeyBoardShown = true
        }
    }

    @objc func kbWillHide(notification: NSNotification) {
        print("kb is not shown")
        if isKeyBoardShown {
            if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                UIView.animate(withDuration: 0.3) {
                    self.buyButton.center.y += keyboardSize.height
                }
            }
            isKeyBoardShown = false
        }

    }
}
