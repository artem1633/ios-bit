//
//  HistoryViewController.swift
//  FACoin
//
//  Created by Алексей Воронов on 01.12.2019.
//  Copyright © 2019 Алексей Воронов. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var hitoryTableView: UITableView!
    let api = ApiWorker.shared
    
    var transactions: [Transaction] = []
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        loadingAnimation(show: true)
        
        hitoryTableView.delegate = self
        hitoryTableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        getTransactoins()
    }
    
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getTransctionsRows(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HistoryTableViewCell
        cell.setup(transaction: getTransaction(section: indexPath.section, row: indexPath.row))
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return getTransactionsSections()
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "ru_RU")
        formatter.dateFormat = "dd MMMM"
        
        let date = getSectionDate(section: section)
        
        return formatter.string(from: date)
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let headerView = view as! UITableViewHeaderFooterView
        headerView.contentView.backgroundColor = .white
        headerView.backgroundColor = UIColor.clear
        headerView.textLabel?.font = UIFont.headerTitleCell
        headerView.textLabel?.textColor = UIColor.TextGrayColor
    }
    
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        let footerView = view as! UITableViewHeaderFooterView
        footerView.contentView.backgroundColor = UIColor.BGColor
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 16
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 46
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let transaction = getTransaction(section: indexPath.section, row: indexPath.row)
        print(transaction)
        if transaction.type == "transfer" {
            //print("https://www.blockchain.com/btc/tx/\(transaction.key)")
            guard let url = URL(string: "https://www.blockchain.com/btc/tx/\(transaction.key)") else { return }
            UIApplication.shared.open(url)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func getTransactoins() {
        api.getTransactions() { (result, error) in
            if error != nil {
                self.showError(text: error!)
            } else {
                self.transactions = result
                self.hitoryTableView.reloadData()
            }
            self.loadingAnimation(show: false)
        }
    }
    
    func getTransactionsSections() -> Int {
        let calendar = Calendar.current
        var days: [DateComponents] = []
        for transaction in self.transactions {
            let dayComponent = calendar.dateComponents([.day], from: transaction.created_at)
            if !days.contains(dayComponent) { days.append(dayComponent)}
        }
        return days.count
    }
    
    func getTransctionsRows(section: Int) -> Int {
        let calendar = Calendar.current
        var days: [DateComponents] = []
        for transaction in self.transactions {
            let dayComponent = calendar.dateComponents([.day], from: transaction.created_at)
            if !days.contains(dayComponent) { days.append(dayComponent)}
        }
        var result: [Transaction] = []
        for transaction in self.transactions {
            let dayComponent = calendar.dateComponents([.day], from: transaction.created_at)
            if days[section] == dayComponent { result.append(transaction)}
        }
        
        return result.count
    }
    
    func getTransaction(section: Int, row: Int) -> Transaction {
        let calendar = Calendar.current
        var days: [DateComponents] = []
        for transaction in self.transactions {
            let dayComponent = calendar.dateComponents([.day], from: transaction.created_at)
            if !days.contains(dayComponent) { days.append(dayComponent)}
        }
        var result: [Transaction] = []
        for transaction in self.transactions {
            let dayComponent = calendar.dateComponents([.day], from: transaction.created_at)
            if days[section] == dayComponent { result.append(transaction)}
        }
        return result[row]
    }
    
    func getSectionDate(section: Int) -> Date {
        let calendar = Calendar.current
        var days: [DateComponents] = []
        for transaction in self.transactions {
            let dayComponent = calendar.dateComponents([.day], from: transaction.created_at)
            if !days.contains(dayComponent) { days.append(dayComponent)}
        }
        
        for transaction in self.transactions {
            let dayComponent = calendar.dateComponents([.day], from: transaction.created_at)
            if days[section] == dayComponent {
                return transaction.created_at
            }
        }
        return Date()
    }
}
