//
//  CardBuyViewController.swift
//  FACoin
//
//  Created by Святослав Шевченко on 18.12.2020.
//  Copyright © 2020 Алексей Воронов. All rights reserved.
//

import UIKit
import WebKit

class CardBuyViewController: UIViewController, WKNavigationDelegate, BackFromPushDelegate {
    func goBack() {
        print("key key")
        self.navigationController?.popViewControllers(viewsToPop: 2)
    }
    
    
    @IBOutlet weak var webView: WKWebView!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    //let aVariable = appDelegate.someVariable
    
    var urlString = ""
    
    override func viewDidLoad() {
        appDelegate.delegate = self
        super.viewDidLoad()
        if let url = URL(string: urlString){
            let request = URLRequest(url: url)
            webView.load(request)
        }
        webView.navigationDelegate = self
    }
}
