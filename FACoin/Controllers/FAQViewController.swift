//
//  FAQViewController.swift
//  FACoin
//
//  Created by Алексей Воронов on 09.12.2019.
//  Copyright © 2019 Алексей Воронов. All rights reserved.
//

import UIKit

class FAQViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    let api = ApiWorker.shared
    var faqs: [(String, String)] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadingAnimation(show: true)

        tableView.dataSource = self
        tableView.delegate = self
        
        api.loadFAQ() { value in
            if value.count == 0 {
                self.showError(text: "Ошибка загрузки")
            } else {
                for i in value {
                    let subject = i["subject"] as? String ?? ""
                    let content = i["content"] as? String ?? ""
                    self.faqs.append((subject, content))
                }
                self.tableView.reloadData()
            }
            self.loadingAnimation(show: false)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return faqs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! FAQUTableViewCell
        cell.label.text = faqs[indexPath.row].0
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let content = faqs[indexPath.row].1
        showAlert(text: content)
    }
}
