//
//  PinViewController.swift
//  FACoin
//
//  Created by Алексей Воронов on 09.12.2019.
//  Copyright © 2019 Алексей Воронов. All rights reserved.
//

import UIKit
import KAPinField
import SwiftMessages

class PinViewController: UIViewController, KAPinFieldDelegate {
    
    let api = ApiWorker.shared
    
    @IBOutlet weak var pinField: KAPinField!
    @IBOutlet weak var titleLabel: UILabel!
    
    var firstPin = ""
    var currentPin = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        
        pinField.backgroundColor = .clear
        pinField.becomeFirstResponder()
        
        pinField.properties.delegate = self
        pinField.properties.isSecure = true
        pinField.properties.secureToken = "•"
        pinField.properties.token = "•"
        pinField.appearance.tokenColor = UIColor.white.withAlphaComponent(0.35)
        
        currentPin = UserDefaults.standard.string(forKey: "pin") ?? ""
        print(currentPin)
        if currentPin == "" {
            titleLabel.text = "Установите ПИН-КОД"
        } else {
            titleLabel.text = "Введите ПИН-КОД"
        }
    }
    
    func pinField(_ field: KAPinField, didFinishWith code: String) {
        if firstPin == "" && currentPin == "" {
            firstPin = code
            pinField.text = ""
            pinField.animateFailure()
            titleLabel.text = "Повторите ПИН-КОД"
        } else if currentPin == "" {
            if code == firstPin {
                UserDefaults.standard.set(code, forKey: "pin")
                api.locked = false
                dismiss(animated: true, completion: nil)
            } else {
                showError(text: "ПИН-КОД должен совпадать!")
                titleLabel.text = "Установите ПИН-КОД"
                firstPin = ""
                pinField.animateFailure()
                pinField.text = ""
            }
        } else if code == currentPin {
            api.locked = false
            dismiss(animated: true, completion: nil)
        } else {
            showError(text: "Неверный ПИН-КОД")
            pinField.animateFailure()
            pinField.text = ""
        }
    }

}
