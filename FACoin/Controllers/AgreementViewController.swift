//
//  AgreementViewController.swift
//  FACoin
//
//  Created by Святослав Шевченко on 18.12.2020.
//  Copyright © 2020 Алексей Воронов. All rights reserved.
//

import UIKit
import WebKit

class AgreementViewController: UIViewController, WKNavigationDelegate {
    
    @IBOutlet weak var webView: WKWebView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let url = URL(string: "http://facoin.ru/info.pdf"){
            let request = URLRequest(url: url)
            webView.load(request)
        }
        webView.navigationDelegate = self
    }
}
