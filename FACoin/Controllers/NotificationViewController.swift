//
//  NotificationViewController.swift
//  FACoin
//
//  Created by Алексей Воронов on 11.12.2019.
//  Copyright © 2019 Алексей Воронов. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    let api = ApiWorker.shared
    var notifs: [NotificationQ] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadingAnimation(show: true)

        tableView.dataSource = self
        tableView.delegate = self
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)

        
        api.getNotif() { value, error in
            if error != nil {
                self.showError(text: error!)
            } else {
                if value.count > 0 {
                    self.notifs = value
                    self.tableView.reloadData()
                }
            }
            self.loadingAnimation(show: false)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! FAQUTableViewCell
        cell.label.text = notifs[indexPath.row].subject
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let content = notifs[indexPath.row].content
        showAlert(text: content)
    }
}
