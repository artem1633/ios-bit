//
//  CardMessageView.swift
//  FACoin
//
//  Created by Алексей Воронов on 22.12.2019.
//  Copyright © 2019 Алексей Воронов. All rights reserved.
//

import UIKit
import SwiftMessages

class CardMessageView: MessageView, UITextFieldDelegate {
    @IBOutlet weak var cardNumberField: UITextField!
    @IBOutlet weak var cardHolderField: UITextField!
    @IBOutlet weak var cardDateField: UITextField!
    @IBOutlet weak var cardCVVField: UITextField!
    
    var actionCallback: (()->())?
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else {
            return true
        }
        let lastText = (text as NSString).replacingCharacters(in: range, with: string) as String

        if cardNumberField == textField {
            textField.text = lastText.format("nnnn nnnn nnnn nnnn", oldString: text)
            return false
        } else if cardCVVField == textField {
            textField.text = lastText.format("NNN", oldString: text)
            return false
        } else if cardDateField == textField {
            textField.text = lastText.format("NN/NN", oldString: text)
            return false
        }
        return true
    }
    
    @IBAction func buttonAction() {
        actionCallback?()
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        
        cardNumberField.delegate = self
        cardCVVField.delegate = self
        cardDateField.delegate = self
    }
}
