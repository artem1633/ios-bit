//
//  FAMessageFieldView.swift
//  FACoin
//
//  Created by Алексей Воронов on 23.12.2019.
//  Copyright © 2019 Алексей Воронов. All rights reserved.
//

import UIKit
import SwiftMessages


class FAMessageFieldView: MessageView, UITextFieldDelegate {
@IBOutlet weak var title: UILabel!
@IBOutlet weak var textField: UITextField!

var actionCallback: (()->())?
    
    @IBAction func buttonAction() {
        actionCallback?()
    }
    
}
